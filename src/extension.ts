"use strict";

import * as net from 'net'
import { commands, workspace, ExtensionContext } from "vscode";
import { ChildProcess, spawn } from "child_process";

import {
    LanguageClient,
    LanguageClientOptions,
    StreamInfo,
    Trace,
} from "vscode-languageclient";

function generateRandomServerPort(): number {
    const minPort = 10000;
    const maxPort = 40000;

    return Math.floor((Math.random() * (maxPort - minPort)) + minPort);
}

// function delay(ms: number): Promise<NodeJS.Timer> {
//     return new Promise( (resolve: (value?: (NodeJS.Timer)) => void) => setTimeout(resolve, ms) );
// }

let client: LanguageClient;
let serverProcess: ChildProcess;

export function activate(context: ExtensionContext) {
    const port = generateRandomServerPort();

    const serverOptions = () =>
        new Promise<ChildProcess | StreamInfo>(async (resolve, reject) => {
            serverProcess = spawn('php', [
                // TODO: Make configurable.
                "-dmemory_limit=2000M",
                __dirname + "/distribution.phar",
                '--uri=tcp://127.0.0.1:' + port
            ]);

            if (serverProcess.stdout === null || serverProcess.stderr === null) {
                throw new Error('Server process was spawned but did not have STDOUT or STDERR pipe due to unknown error');
            }

            serverProcess.stdout.on('data', (data: Buffer) => {
                const message = data.toString();

                console.debug('The PHP server has something to say:', message);

                if (message.indexOf('Starting server bound') !== -1) {
                    // Assume the server has successfully spawned the moment it says it's listening.
                    const clientConnection = net.createConnection(port);

                    clientConnection.on('data', (chunk: Buffer) => {
                        const str = chunk.toString();
                        console.log(str);
                    });

                    resolve({ reader: clientConnection, writer: clientConnection })
                }
            });

            serverProcess.stderr.on('data', (data: Buffer) => {
                console.error('The PHP server has errors to report:', data.toString());
            });

            serverProcess.on('close', (code) => {
                if (code === 2) {
                    console.error(`Port ${port} is already taken`);
                } else if (code !== 0 && code !== null) {
                    const detail =
                        'Serenata unexpectedly closed. Either something caused the process to stop, it crashed, ' +
                        'or the socket closed. In case of the first two, you should see additional output ' +
                        'indicating this is the case and you can report a bug. If there is no additional output, ' +
                        'you may be missing the right dependencies or extensions or the server may have run out ' +
                        'of memory (you can increase it via the settings screen).';

                    console.error(detail);
                }

                reject();
            });
        });

    const config = workspace.getConfiguration('serenata');

    let clientOptions: LanguageClientOptions = {
        // Register the server for plain text documents
        documentSelector: [
            {
                pattern: "**/*.php",
                scheme: "file"
            },
        ],
        synchronize: {
            configurationSection: "serenata",
            fileEvents: workspace.createFileSystemWatcher("**/*.php")
        },
        initializationOptions: {
            configuration: config.get<object>('configuration'),
        }
    };

    // Create the language client and start the client.
    client = new LanguageClient(
        "serenata",
        "Serenata",
        serverOptions,
        clientOptions
    );

    client.clientOptions.errorHandler;
    client.trace = Trace.Verbose;
    let disposable = client.start();

    // The server requests to execute this command on the server, so just forward it.
    commands.registerCommand('serenata/command/openTextDocument', () => {
        // TODO: Seem to be missing the data to pass to the command. We need to forward this to the server and it is
        // part of the spec.
        console.log("Code lens command triggered", arguments);

        // client.sendRequest('workspace/executeCommand', {
        //     command: 'serenata/command/openTextDocument', // Same as one we got, just forward.
        //     arguments: arguments,
        // });
    });

    // In response to commands such as openTextDocument, the server might request to open a text document.
    client.onRequest('serenata/openTextDocument', (parameters) => {
        console.log("Received request to open text document", parameters);

        /*
            TODO: Should be roughly equivalent of

            atom.workspace.open(Convert.uriToPath(parameters.uri), {
                initialLine: parameters.position.line,
                searchAllPanes: true,
            });
        */
    });

    // Push the disposable to the context's subscriptions so that the
    // client can be deactivated on extension deactivation
    context.subscriptions.push(disposable);
}

export function deactivate(): Thenable<void> {
    if (!client) {
        return new Promise((resolve) => {
            resolve();
        });
    }

    return client.stop().then(() => {
        serverProcess.kill();
    });
}
